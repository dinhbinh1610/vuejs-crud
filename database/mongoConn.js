import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost:27017/dbinh')

let db = mongoose.connection

db.on('err', function (err) {
  console.log('connect err' + err)
})

db.once('open', function () {
  console.log('MongoDB connect success')
})

export default mongoose
