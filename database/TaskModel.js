import BaseModel from './BaseModel'

class TaskModel extends BaseModel {
  constructor () {
    super()
    this.table = 'tasks'
    this.schema = {
      name: String,
      status: String,
      created_time: Number
    }
    this.model = this.getModel()
  }

  getTask (query = {}) {
    return this.model.find(query).exec()
  }

  getOneTask (query = {}) {
    return this.model.findOne(query).exec()
  }

  createTask (name) {
    let Task = this.model
    let newTask = new Task({
      name: name || 'Noname',
      status: 'not-done',
      created_time: Date.now()
    })
    return newTask.save()
  }

  updateTask (id, newTask) {
    return this.model.findOneAndUpdate({_id: id}, newTask).exec()
  }

  deleteTask (id) {
    return this.model.findOne({_id: id}).remove().exec()
  }
}

export default new TaskModel()
