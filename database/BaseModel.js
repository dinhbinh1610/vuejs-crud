import mongoose from './mongoConn'

export default class BaseModel {
  getModel () {
    console.log('this.table, this.schema', this.table, this.schema)
    return mongoose.model(this.table, this.schema)
  }
}
