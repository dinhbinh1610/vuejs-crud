import Vue from 'vue'
import Router from 'vue-router'
import ListTask from '@/components/ListTask'
import CreateTask from '@/components/CreateTask'
import EditTask from '@/components/EditTask'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'ListTask',
      component: ListTask
    },
    {
      path: '/create',
      name: 'CreateTask',
      component: CreateTask
    },
    {
      path: '/:id/edit',
      name: 'EditTask',
      component: EditTask
    }
  ]
})
