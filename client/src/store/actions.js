import {API_HOST} from '../../config/host'
import Vue from 'vue'

export default {
  getTask: ({commit}) => {
    Vue.http.get(`${API_HOST}/task`)
      .then(({body}) => {
        commit('GET_TASK', {tasks: body})
      })
  },
  getOneTask: ({commit}, {id}) => {
    Vue.http.get(`${API_HOST}/task/${id}`)
      .then(({body}) => {
        commit('GET_ONE_TASK', {id, task: body})
      })
  },
  createTask: ({commit}, {task}) => {
    Vue.http.post(`${API_HOST}/task`, task)
      .then(({body}) => {
        commit('CREATE_TASK', {task: body})
      })
  },
  updateTask: ({state, commit}, {id, updateData}) => {
    let data = {
      task: updateData
    }
    Vue.http.post(`${API_HOST}/task/${id}`, data)
      .then(({body}) => {
        commit('UPDATE_TASK', {id, data: updateData})
      })
  },
  deleteTask: ({ commit }, {id}) => {
    Vue.http.delete(`${API_HOST}/task/${id}`)
      .then(({body}) => {
        commit('DELETE_TASK', {id})
      })
  }
}
