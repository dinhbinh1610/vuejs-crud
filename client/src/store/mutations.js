export default {
  GET_TASK (state, {tasks}) {
    state.tasks = tasks
  },
  GET_ONE_TASK (state, {id, task}) {
    state.tasks = [
      ...state.tasks.filter(task => task._id !== id),
      task
    ]
  },
  CREATE_TASK (state, {task}) {
    state.tasks = [
      ...state.tasks,
      task
    ]
  },
  DELETE_TASK (state, { id }) {
    state.tasks = state.tasks.filter(task => task['_id'] !== id)
  },
  UPDATE_TASK (state, {id, data}) {
    state.tasks = state.tasks.map(task => {
      if (task['_id'] === id) {
        task = Object.assign(task, data)
      }
      return task
    })
  }
}
