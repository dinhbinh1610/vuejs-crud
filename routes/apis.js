import TaskModel from '../database/TaskModel'
var router = require('express').Router()

router.get('/task', function (req, res, next) {
  TaskModel.getTask()
    .then((data) => {
      res.send(data)
    })
    .catch(err => {
      console.log('find catch err', err)
    })
})

router.get('/task/:id', function (req, res, next) {
  let id = req.params.id
  TaskModel.getOneTask({_id: id})
    .then((data) => {
      res.send(data)
    })
    .catch(err => {
      console.log('find catch err', err)
    })
})

router.post('/task', function (req, res, next) {
  console.log('call 1')
  let taskName = req.body.name
  TaskModel.createTask(taskName)
    .then((data) => {
      res.send(data)
    })
    .catch(err => {
      console.log('created err', err)
    })
})

router.post('/task/:id', function (req, res, next) {
  let taskId = req.params.id
  let task = req.body.task
  TaskModel.updateTask(taskId, task)
    .then((data) => {
      res.send(data)
    })
    .catch(err => {
      console.log('updated err', err)
    })
})

router.delete('/task/:id', function (req, res, next) {
  let taskId = req.params.id
  console.log('taskId', taskId)
  TaskModel.deleteTask(taskId)
    .then((data) => {
      res.send(data)
    })
    .catch(err => {
      console.log('deleted err', err)
    })
})

module.exports = router
