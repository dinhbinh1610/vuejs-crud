# server

``` bash
# config Db address
./database/mongoConn.js


# stash web app
npm start

```

# client

``` bash

# serve with hot reload at localhost:8080
cd client
npm run dev

```

